#-*- coding: utf-8 -*

import time
import md5
import pickle
import const
from os.path import isfile
import urllib

def getUserId():
    if isfile(const.userIdFile):
        pkl_file = open(const.userIdFile, 'r')
        uid = pickle.load(pkl_file)
        pkl_file.close()
    else:
        uid=md5.new(str(time.time()) ).hexdigest()
        pckfile=open(const.userIdFile,"w+")
        pickle.dump(uid,pckfile)
        pckfile.close()
    return uid

def getGrades():
    if isfile(const.gradesFile):
        pkl_file = open(const.gradesFile, 'r')
        grades = pickle.load(pkl_file)
        pkl_file.close()
        return grades;
    else:
        return {}
        
def sendGrade(drinkid,grade):
    sendUrl=const.gradeUrl % (const.userId,drinkid,grade,const.xmlid,const.appid) 
    urllib.urlopen(sendUrl)


def saveGrades():
    pckfile=open(const.gradesFile,"w+")
    pickle.dump(const.grades,pckfile)
    pckfile.close()

def getObjectWithId(breweries,id):
    """Hämta objektet från bryggerier med id"""
    for brewery in breweries:
        if brewery.id==id:
            return brewery
        for drink in brewery.drinks:
            if drink.id==id:
                return drink
    return None

def getDrinkWithId(breweries,id):
    """Hämta dryck-objekt med id.  

    Vi går igenom alla bryggerier för att hitta drycker.

    """
    for brewery in breweries:
        for drink in brewery.drinks:
            if drink.id==id:
                return drink
    return None


def getBreweryWithId(breweries,id):
    """Hämta bryggeri-objekt med id."""
    for brewery in breweries:
        if brewery.id==id:
            return brewery
    return None
