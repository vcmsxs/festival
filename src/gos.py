#-*- coding: utf-8 -*
"""Den inhåller även ett par dummy-hjälp-klasser. För att ladda ner
och parsa xml från Gefle Ölsellskaps hemsida.

"""
from kivy.uix.treeview import (TreeView,TreeViewLabel)
from kivy.uix.button import Button
import xml.etree.ElementTree as etree
import urllib
import util
from os.path import isfile
import const
 
class Drink():
    pass

class Brewery():
    pass

class TreeViewButton(Button, TreeViewLabel):
    def __init__(self,typ,id,**var):
        TreeViewLabel.__init__(self,**var)
        Button.__init__(self,**var)
        self.typ=typ
        self.id=id

class XmlCrawler():
    def __init__(self,parent,fileurl):
        """Läser in xml från fileurl, parsar och lägger resulatet i listan
        bryggerier

        """
        self.parent=parent
        
        try:
            xml = urllib.urlopen(fileurl)
            const.local_file=False
        except:
            xml = open(const.defxmlfile,"r");
            const.local_file=True
   
        root = etree.fromstring(xml.read())
        breweries=self.breweries=self.getBreweryList(root);
        
        
    def getBreweryList(self,root):
        """Går igenom root-elementet och poppulerar ett dummy-objekt bryggeri
        som läggs till i medlemslistan bryggerier 
        """
        breweries=[]
        for brewery in root:
            brewery_obj=Brewery()
            brewery_obj.id=brewery.attrib["id"]
            brewery_obj.namn=brewery.find("namn").text
            brewery_obj.url=brewery.find("url").text
            brewery_obj.land=brewery.find("land").text
            brewery_obj.ort=brewery.find("ort").text
            brewery_obj.drinks=self.getDrinkList(brewery)
            breweries.append(brewery_obj)
        return breweries
        
    def getDrinkList(self,brewery):
        """Går ingeom bryggeri-node och letar efter dryck under drycker. För
        varje dryck poppulerars ett dummy object som läggs till i
        dryck.

        """
        drinks=[]
        for drink in brewery.find("drycker").findall("dryck"):
            drink_obj=Drink()
            drink_obj.id=drink.attrib["id"]
            drink_obj.namn=drink.find("namn").text
            drink_obj.typ=drink.find("typ").text
            drink_obj.alkoholhalt=drink.find("alkoholhalt").text
            drink_obj.ibu=drink.find("ibu").text;
            drinks.append(drink_obj)
        return drinks

    def getTreeView(self):
        """Poppulerar en kivy TreeView med bryggerilistan 
        """
        treeview=TreeView(hide_root=True,size_hint=(1,0.9))
        
        for b in self.breweries: 
            brewerylabel=TreeViewButton(typ="b",id=b.id,text=b.namn,background_color=[1,1,1,0.3])
            brewerylabel.bind(on_press=self.my_callback, state=self.state_callback)
            brewerynode=treeview.add_node(brewerylabel)
            for d in b.drinks:              
                drinkLabel=TreeViewButton(typ="d",id=d.id,text=d.namn,background_color=[1,0,1,0.5])
                drinkLabel.bind(on_press=self.my_callback, state=self.state_callback)
                treeview.add_node(drinkLabel,brewerynode)
        return treeview

    def state_callback(self, obj, value):
        pass

    def my_callback(self, obj):
        # print('press on button', obj.id)
        self.parent.openModal(util.getObjectWithId(self.breweries,obj.id))
        
