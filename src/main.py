#-*- coding: utf-8 -*

""" Main application module
"""

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.modalview import ModalView
from kivy.uix.scrollview import ScrollView 
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.carousel import Carousel
from kivy.factory import Factory
from kivy.uix.slider import Slider
from gos import (XmlCrawler,  Brewery,  Drink)
import webbrowser
import util
import const

__window__= Window
__version__ = '0.2'

class FestivalApp(App):
    def build(self):
        self.crawler = XmlCrawler(self, const.xmlurl_test)  
                
        # Hämta App-kattalogen,  där vi får skriva filer.
        const.userdir = super(FestivalApp, self).user_data_dir
        
        # Lägg till app-kattalogen till filerna vi skall ladd och spara
        const.gradesFile = const.userdir + "/" +const.gradesFile
        const.userIdFile = const.userdir + "/" +const.userIdFile

        #Hämta uppgifter från filer eller skapa initieringsvärden
        const.userId = util.getUserId()
        const.grades = util.getGrades()
        const.appid = __version__
        
        #Sätt upp den grafiska grund-designen
        carousel = Carousel(direction='right')
        carousel.add_widget(self.IntroPage())
        carousel.add_widget(self.BeerListPage())
        carousel.add_widget(self.InfoPage())
        return carousel

    def getHeadLine(self, subtext):
        labelText =  Label(markup =  True, halign =  "center", size_hint =  (1, 0.2))
        labelText.text = const.headline_with_sub%(subtext)
        return labelText

    def getBgImg(self):
        image  =  Factory.AsyncImage(source = const.bgImgFile)
        image.allow_stretch =  True
        image.color =  [1, 1, 1, 0.2]
        return image
        
    def webSurfBrewery(self, instance, value):
        """Öppna webbrowsern till ett bryggeri"""
        webbrowser.open(self.modalBrewery.url)        
        
    def webSurfGoogleMap(self, instance,  value):
        """Öppna webbrowsern till google map"""
        webbrowser.open(const.mapUrl)
        print "instance " + instance
        print "value " + value

    def openModal(self, obj):
        """ Öppna ett modalt fönster för bryggeri eller dryck"""
        if isinstance(obj, Drink):
            self.openModalDrink(obj)
        elif isinstance(obj, Brewery):
            self.openModalBrewery(obj)

    def openModalBrewery(self, brewery):
        """Öppna modalt fönster för att visa bryggeri-info """
        self.modalBrewery =  brewery
        self.breweryView = view =  ModalView(size_hint=(0.8, 0.8),
                          background_color =[0, 0, 0, 0.8])
        box =  BoxLayout(orientation="vertical")
        box.add_widget(Label(halign="center", 
                             text =  "BRYGGERI",
                             size_hint=(1,0.1)))
        content = const.modalBreweryText % (brewery.namn, brewery.land, brewery.ort)
        contentLabel = Label(spaceing =  10, 
                             halign =  "left", 
                             text =  content, 
                             markup =  True)
        contentLabel.bind(on_ref_press = self.webSurfBrewery)

        closeButton = Button(text = "Stäng fönster",size_hint=(1,0.1))
        closeButton.bind(on_press=self.closeBrewery,  state=self.dummy_callback)

        box.add_widget(contentLabel)
        box.add_widget(closeButton)

        view.add_widget(box)
        view.open()

    def closeBrewery(self, obj):
        self.breweryView.dismiss(force=True)


    def change(self, value, instance):
        self.contentLabel.text = const.drink_text % (self.modalDrink.namn, 
                                             self.modalDrink.typ, 
                                             self.modalDrink.alkoholhalt, 
                                             self.modalDrink.ibu, 
                                             self.slider.value)
       
    def openModalDrink(self, drink):
        """ Öppna modalt fönster för att visa dryck och sätta betyg"""
        self.modalDrink = drink
        self.view = view  =  ModalView(size_hint=(0.8, 0.8), 
                                       background_color=[0, 0, 0, 0.8])

        box = BoxLayout(orientation = "vertical")
        box.add_widget(Label(halign = "center", 
                             text = "DRYCK",
                             size_hint=(1,0.1)))

        #Kanske har vi satt betyg förut,  då är det bäst att vi kolla
        #upp det.
        if self.modalDrink.id in const.grades:
            value = const.grades[self.modalDrink.id]
        else: value = 0

        self.slider = slider = Slider(min = 0, max = 10, 
                                      value = value, 
                                      size_hint=(0.8,1))

        #Vi anävnder an anchor för att centrera slidern
        slider_layout = AnchorLayout(anchor_x='center', anchor_y='center')
        slider_layout.add_widget(self.slider)
 
        self.contentLabel = Label(spaceing = 10, 
                                  halign = "left", 
                                  markup = True)
        contentLabel = self.contentLabel
        contentLabel.text = const.drink_text % (drink.namn, 
                                        drink.typ, 
                                        drink.alkoholhalt, 
                                        drink.ibu, 
                                        slider.value)

        box.add_widget(contentLabel)

        slider.bind(value = self.change)
        box.add_widget(slider_layout)        
        
        cancelButton = Button(text = "Avbryt",size_hint=(1,0.1))
        saveButton = Button(text = "Spara",size_hint=(1,0.1))
        saveButton.bind(on_press=self.saveGrades, state=self.dummy_callback)
        cancelButton.bind(on_press=self.cancelGrades,  state=self.dummy_callback)
        box.add_widget(saveButton)
        box.add_widget(cancelButton)
        view.add_widget(box)
        view.open()
    

    def dummy_callback(self,  obj,  value):
        pass

    def saveGrades(self, obj):
        const.grades[self.modalDrink.id]  =  int(self.slider.value)
        util.saveGrades()
        util.sendGrade(self.modalDrink.id, int(self.slider.value))
        self.view.dismiss(force=True)

    def cancelGrades(self, obj):
        self.view.dismiss(force=True)


    def IntroPage(self):
        floatlayout = FloatLayout()
        floatlayout.add_widget(self.getBgImg())

        box  =  BoxLayout(orientation="vertical")
        box.add_widget(self.getHeadLine("Information"))


        infoText = Label(markup=True, size_hint=(1, 0.9))
        infoText.text = const.intro_text
        infoText.bind(on_ref_press = self.webSurfGoogleMap)

        box.add_widget(infoText)

        if const.local_file==True:
            warning=Label(text="[color=#FF0000]INGEN NÄTKONTAKT !![/color]\nStarta nätverk och starta om Appen.",size_hint=(1,0.15),markup=True)
            box.add_widget(warning)
            

        floatlayout.add_widget(box)
        
        return floatlayout


    def BeerListPage(self):
        floatlayout = FloatLayout()
        floatlayout.add_widget(self.getBgImg())

        box  =  BoxLayout(orientation="vertical", spacing=10)
        box.add_widget(self.getHeadLine("Bryggare och Drycker"))


        tv  =  self.crawler.getTreeView()
        tv.size_hint  =  1,  None
        tv.bind(minimum_height  =  tv.setter('height'))

        scroll  =  ScrollView(pos =(0,  0))
        scroll.add_widget(tv)

        
        box.add_widget(scroll)       
        floatlayout.add_widget(box)
        return floatlayout


    def InfoPage(self):
        floatlayout = FloatLayout()
        floatlayout.add_widget(self.getBgImg())

        box  =  BoxLayout(orientation="vertical", spacing=10)
        floatlayout.add_widget(box)

        box.add_widget(self.getHeadLine("Samarbetspartners"))

        box_partners = BoxLayout(orientation="vertical", size_hint=(1, .9))
        box.add_widget(box_partners)

        label = Label(text=const.partner_text, markup="True")
        label.bind(on_ref_press = self.pressedPartner)
        
        box_partners.add_widget(label);
        
        return floatlayout
        
    def pressedPartner(self, instance, value):
        """Öppna webbrowsern till en parter"""
        webbrowser.open(const.partner_url[str(value)])        


                
if __name__ == '__main__':
    FestivalApp().run()
    
