#-*- coding: utf-8 -*

"""Constant module.  Här skall alla "globala" variabler ligga. Det
finns ett par undantag. Bl.a. __version__ i main app.

"""

breweries=[]
grades={} # drink.id:grade

gradesFile="grades"
userIdFile="userId"

bgImgFile= "media/glassofbeer_h200mm.png"

userdir=""


gradeUrl="http://gefleolsellskap.com/finolsfestival/rate.php?uid=%s&bid=%s&betyg=%i&xid=%s&aid=%s"


xmlurl="http://gefleolsellskap.com/finolsfestival/list.xml"
xmlurl_test="http://gefleolsellskap.com/finolsfestival/_test_list.xml"

defxmlfile="default.xml"

mapUrl="http://goo.gl/maps/Rrv8S"
headline_with_sub="[size=50][b]FINÖLSFESTIVAL![/b][/size]\n[size=20]%s[/size]"

xmlid="0.1"


drink_text=u"""
        Namn: %s
        Typ: %s\
        Alkoholhalt: %s
        IBU: %s\n
        Dra och klicka OK för att sätta betyg
        0=vet ej, 1 är lägsta betyg, 10 är högsta.\n
        Betyg: [color=#FF0000][size=40][b]%d[/b][/size][/color]
        """
intro_text="""[b]VAR?:[/b]
        Gasklockorna i Gävle
        Atlasgatan 3
        [ref=map][color=#FF0000]Klicka [b]HÄR[/b][/color][/ref] för karta.


[b]NÄR?[/b]
        Fredagen 30 Maj Klockan 17:00 - 01:00

        Lördagen 31 Maj Klockan 12:00 - 01:00


     
        Dra med fingret för att navigera. 
        """


partner_text="""[b]Klicka för mer information.[/b]


[ref=kommun]Gävle kommun[/ref]

[ref=gavligt]Gävligt Gott[/ref]

[ref=knowit]Knowit Gävleborg AB[/ref]

[ref=studie]Studiefrämjandet[/ref]

[ref=terrass]Terrassen Pub & Kök[/ref]

[ref=osterns]Österns Pärla[/ref]
"""

partner_url={"osterns":"http://www.osternsparla.nu/",
             "studie":"http://www.studieframjandet.se/",
             "gavligt":"http://www.gavligtgott.se/",
             "terrass":"http://terrassenpub.se/",
             "knowit":"http://www.knowit.se",
             "kommun":"http://www.gavle.se"
        }


modalBreweryText=u"""Namn: [b]%s[/b]\n

        Land: %s

        Ort: %s


        [ref=url][color=#FF0000][b]Klicka HÄR[/b] 
 för att komma till websida för websida[/color][/ref]\n

        """
