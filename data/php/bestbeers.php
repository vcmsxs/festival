
<html>
<head>
<title>De 10 bästa ölen</title>
<meta charset="utf-8">
</head>
<body>

<?php

$drycker_array=array();
$best_array=array(); 

function loadXml($xmlfile)
{
  //Parsa xml-filen, plocka fram dryckes-arrayerna

  $xml =  simplexml_load_file($xmlfile); 

  //Gå igenom alla bryggier, plocka namnet på bryggeriet och drycker-arrayen
  foreach ($xml as $bryggeri)
    {
      $dryck=array();
      $bryggarenamn=$bryggeri->namn;
      getDrycker($bryggarenamn,$bryggeri->drycker); 
    }

}


function getDrycker($bryggerinamn,$drycker)
{
  //Gå igenom drycker arrayen och plocka drycker.
 
 global $drycker_array;
  foreach($drycker as $dryck_xml)
    { // Varför måste vi ingenom den här loopen? Verkar inte logiskt.
      foreach($dryck_xml->dryck as $dryck)
	{
	  $dryck_array = array();
	  $dryck_array["bryggeri"]=(string)$bryggerinamn;
	  $dryck_array["dryck"]=(string)$dryck->namn;
	  $drycker_array[(string)$dryck->attributes()->id]=$dryck_array;
	}      
    }
}
  
function getAvrageTable()
{
  //Skapa en array med ölid och snittbetyg

  $avgRows=array();
  $con=new mysqli ("localhost","gefleolsellskap","RabarberSoppa0099","gefleolsellskap");    
  if (mysqli_connect_errno($con))
    {
      echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

  $sql = "SELECT beerid, round(avg(betyg),2) as snittbetyg from festival_betyg group by beerid order by snittbetyg DESC ";
  
  if ($result = mysqli_query($con, $sql)) 
    {
      while ($row = mysqli_fetch_assoc($result)) 
	{
	  $avgRows[]=$row;
   	}
    mysqli_free_result($result);
    }
  else
    {
      echo  mysqli_error($con);
    }

  return $avgRows;
}


function makeBestArray($xml)
{
  //Gå igenom arrayen med ölid och snittbetyg från databasen. Byt ut
  //öl-id mot bryggeri och dryck-namn från xml.
  //
  // Det här är den enda metod man skall behöva köra innan man skapar
  // html-tabellen.
  global $best_array,$drycker_array;
  loadXml($xml);
  $avg=getAvrageTable();
  foreach ($avg as $row)
    {
      $bestrow=array();
      $bestrow["bryggeri"]=$drycker_array[$row["beerid"]]["bryggeri"];
      $bestrow["dryck"]=$drycker_array[$row["beerid"]]["dryck"];
      $bestrow["snittbetyg"]=$row["snittbetyg"];
      $best_array[]=$bestrow;
    }
}

makeBestArray("./list.xml");

?>


<center><h1>De 10 BÄSTA ÖLEN</h1></center><br>


<center>
<table border="1" cellpadding="10">
<th>BRYGGERI</th>
<th>DRYCK</th>
<th>SNITTBETYG</th>

<?php
  $count=0;
foreach($best_array as $row)
  {
    $count++;
    if ($count>10) break;
    echo "<tr>";
    echo "<td>".$row["bryggeri"]."</td>";
    echo "<td>".$row["dryck"]."</td>";
    echo "<td>".$row["snittbetyg"]."</td>";     
    echo "</tr>";
  }
?>
</table>
</center>
</body>

</html>
