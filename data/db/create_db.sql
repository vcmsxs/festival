
DROP TABLE festival_betyg;

CREATE TABLE festival_betyg (  userid VARCHAR(40), 
       	     		       beerid VARCHAR(6),
			       betyg  INT,
			       xmlid  VARCHAR(3),
			       appid  VARCHAR(3)
			    );

CREATE UNIQUE INDEX user_beer ON festival_betyg (userid,beerid);

